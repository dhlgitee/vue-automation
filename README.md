# vue-automation

## 这是什么

一款开箱即用的 Vue 项目模版，基于 Vue CLI

## 特点

- 默认集成 vue-router 和 vuex
- 全局 SASS 资源自动引入
- 精灵图自动生成
- 全局组件自动注册
- 轻松实现团队代码规范

## 文档

[在线文档](http://eoner.gitee.io/vue-automation)

## 支持

给个小 ❤️ 吧~

[![star](https://gitee.com/eoner/vue-automation/badge/star.svg?theme=dark)](https://gitee.com/eoner/vue-automation/stargazers)

## 捐助

微信 & 支付宝

<img src="http://eoner.gitee.io/vue-automation/images/reward-wechat.jpg" width="200" height="200" />
<img src="http://eoner.gitee.io/vue-automation/images/reward-alipay.jpg" width="200" height="200" />

## 交流群

如果我们有群的话~
